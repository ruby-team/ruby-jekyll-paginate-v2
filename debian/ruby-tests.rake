require 'gem2deb/rake/testtask'
  
Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['specs']
  t.test_files = FileList['spec/**/*_spec.rb']
  t.verbose = true
  t.warning = true
end
